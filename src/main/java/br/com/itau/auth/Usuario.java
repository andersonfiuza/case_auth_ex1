package br.com.itau.auth;


import com.sun.jdi.PrimitiveValue;

public class Usuario {

    private Integer id;

    private  String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
