package br.com.itau.auth;


import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.Authenticator;

@RestController
public class UsuarioController {



    @GetMapping("/")
    public Usuario create (@AuthenticationPrincipal Usuario usuario) {

        usuario.setId(usuario.getId());
        usuario.setName(usuario.getName());
        return usuario;

    }

}
